extends Node2D

onready var animation = $AnimationPlayer
onready var timer = $Timer

func _ready() -> void:
	modulate.a = 0
	animation.current_animation = "start"
	animation.play()

func _on_AnimationPlayer_animation_finished(anim_name: String) -> void:
	if anim_name == "start":
		timer.wait_time = randf() + 0.5
		timer.start()
	elif anim_name == "end":
		get_parent().remove_child(self)
		queue_free()


func _on_Timer_timeout() -> void:
	animation.current_animation = "end"
	animation.play()
