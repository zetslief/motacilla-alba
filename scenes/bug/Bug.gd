extends Node2D

export(String) var follow_storage_group = "flowers_storage"
export(Vector2) var speed = Vector2(40.0, 40.0)

onready var follow_storage: Node = get_tree().get_nodes_in_group(follow_storage_group)[0]

var eating = false

const MIN_DISTANCE := 1.0

func _physics_process(delta: float) -> void:
	if eating:
		rotation_degrees += (randf() - 0.5) * 2.0
		return
	var nearest = null
	var distance := 1e6
	for target in follow_storage.get_children():
		if not target.is_free:
			continue
		var new_distance = (target.global_position - global_position).length()
		if new_distance < distance:
			nearest = target
			distance = new_distance
	if nearest:
		if distance > MIN_DISTANCE:
			look_at(nearest.global_position)
			global_position += speed * (nearest.global_position - global_position).normalized() * delta
		else:
			eating = true
			nearest.is_free = false
