extends Node2D

onready var shadow = $Shadow

var last_mouse_position = Vector2.ZERO

var r = 120
var current_angle = 0
var angle_speed = 0.7

func _process(_delta: float) -> void:
	var mouse_position = get_global_mouse_position()
	if last_mouse_position != mouse_position:
		last_mouse_position = mouse_position
		global_position = last_mouse_position

func _physics_process(delta: float) -> void:
	var x = r * cos(current_angle) / scale.x
	var y = r * sin(current_angle) / scale.y
	current_angle += angle_speed * delta
	shadow.position = Vector2(x, y)
	shadow.material.set_shader_param("light", Vector2(x * scale.x, -y * scale.y))
	shadow.material.set_shader_param("thickness", 1.0)
