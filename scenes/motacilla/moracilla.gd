extends KinematicBody2D

export var input_acceleration := Vector2(2.0, 3.0)

export var speed := Vector2(1.0, 1.0)
export var gravity := Vector2(10.0, 10.0)

var velocity := Vector2(1.0, 0.0)
var gravity_enabled = false

var input := Vector2.ZERO

func _process(_delta: float) -> void:
	input = Vector2.ZERO
	if Input.is_action_pressed("left"):
		input.x -= 1.0
	if Input.is_action_pressed("right"):
		input.x += 1.0
	if Input.is_action_pressed("jump"):
		input.y = 1.0

func _physics_process(delta: float) -> void:
	velocity += input * input_acceleration
	velocity = move_and_slide(velocity * speed, Vector2.UP, false, 4)
	velocity /= speed
	velocity.x += -sign(velocity.x) * gravity.x * delta
	velocity.y += gravity.y * delta
	velocity.y = clamp(velocity.y, -gravity.y, gravity.y)
