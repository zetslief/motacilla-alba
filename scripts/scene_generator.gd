extends Position2D

export(NodePath) var storage_path
export(PackedScene) var scene

onready var background_storage = get_node(storage_path)

func _ready() -> void:
	randomize()

func generate() -> void:
	if not scene:
		return
	var instance = scene.instance()
	background_storage.add_child(instance)
	var viewport = get_viewport_rect()
	instance.scale *= 0.5
	instance.global_position = Vector2(
		global_position.x - (randf() - 0.5) * viewport.size.x,
		global_position.y - (randf() - 0.5) * viewport.size.y
	)
	print("Add instance to ", instance.global_position)
